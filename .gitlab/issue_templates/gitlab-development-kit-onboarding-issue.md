This onboarding issue is for setting up GDK on your machine. GitLab Development Kit (GDK) provides a local environment for developing GitLab and related projects.

## GitLab Development Kit

Get started with GitLab Rails development using the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit). The GitLab development Kit contains a monorepo of various key repositories for the GitLab organization. The product lives under the `gitlab/` directory of the `gitlab-development-kit`. 

- [ ] Find a buddy outside Digital Experience to help you onboard to the GDK and schedule a bi-weekly meeting with them.
- [ ] Set it with [one line installation](https://gitlab.com/gitlab-org/gitlab-development-kit#supported-methods) which:
    
    - Clones the GDK project into a new `gitlab-development-kit` directory in the current working directory.
    - Installs `asdf` and necessary `asdf` plugins.
    - Runs `gdk install`.
    - Runs `gdk start`.

    - First follow [dependency installation instructions](index.md#install-prerequisites) if necessary.
```shell
curl "https://gitlab.com/gitlab-org/gitlab-development-kit/-/raw/main/support/install" | bash
```  

Note that this commands can take anywhere from thirty minutes to a couple of hours to complete. This command is designed to be re-run multiple times. If you are running into issues, contuning to `curl` against the install script may resolve your issues. 

- [ ] To browse your developmenet server [Learn more here](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/browse.md) use:  
```shell
cd gitlab-development-kit
gdk start
```
- [ ] [Use the login credentials found here and learn more GDK commands](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/gdk_commands.md)
- [ ] [Read how to use GDK documentation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/index.md)
- [ ] Paste a screenshot of your logged-out http://localhost:3000/ GDK instance in this issue
- [ ] Paste a screenshot of your logged-in instance. The first time logging in, you may be asked to reset password upon logging in the first time
- [ ] Schedule a coffee chat with @tywilliams to talk about the GDK

## Resource

Good slack channels to ask for help and to find onboarding buddy
- #gdk
- #backend
- #development
- #frontend

Handbook Resources
- https://about.gitlab.com/handbook/engineering/development/dev/create-editor/developer-cheatsheet/ 

Docs
- https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main/doc  
- https://docs.gitlab.com/ee/development/
  - https://about.gitlab.com/handbook/engineering/development/principles/
  - https://docs.gitlab.com/ee/development/contributing/style_guides.html

## Advice for keeping your MR progress smooth
DEX folks will need to know that the product repo is *huge*. This means that changes are much slower and therefore it takes longer for things to get merged. Add the appropriate reviewers according to the reviewer roulette. Add  `frontend`/ `backend`/ `security` in accordance to the change. Add detailed instructions on how to successfully review the MR. Videos can help here, too. Watch out for linting errors


