We are so excited you're here! 

The onboarding issue is divided into various tasks to get you set up as an Engineer with the Digital Experience team. If you can't move forward with tasks please reach out to your onboarding buddy from the Digital Experience team.

This is a guide that should help you get started with the Digital Experience team at GitLab and some of the most important tools we use.

Your Digital Experience onboarding buddy is `insert-handle`. 

Start this issue after completing your GitLab onboarding issue here: `GitLab Onboarding Issue`

## Buyer Experience repository

This is the new repository used for generating webpages built with Nuxt on the about.gitlab.com website. You will need to set the repository up locally and contribute to it as part of your job function with this team. 

- [ ] Clone the repository https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience
- [ ] Read the root `README.md` and `/docs` at https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/tree/main/docs
- [ ] Follow the instructions to set up locally https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/blob/main/README.md

## Slippers Design System

This is the repository used for our design system. You will need to set the repository up locally and contribute to it as part of your job function with this team. 

- [ ] Clone the repository https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui
- [ ] Read the root `README.md` 
- [ ] Follow the instructions to set up locally on `README.md`

## www-gitlab-com repository

This is the primary repository used for creating the about.gitlab.com website. You will need to set the repository up locally and contribute to it as part of your job function with this team. 

- [ ] Clone the repository https://gitlab.com/gitlab-com/www-gitlab-com 
- [ ] Read the root `README.md` and `/docs` at https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/doc 
- [ ] Follow the instructions to set up locally https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/doc/development.md

## Add yourself to our team page in the handbook using the www-gitlab-com repository

- [ ] create a unique branch name. Something like `leantech-yourname-onboarding-date`
- [ ] add yourself as a team member to our [Digital Experience handbook webpage](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/#team) . [Here's a direct link to this file in our repo if you need help finding it.](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/sites/handbook/source/handbook/marketing/inbound-marketing/digital-experience/index.html.md.erb) 
- [ ] Push our branch up to origin, and create a merge request. Fill out the merge request description with what was changed and why. 
- [ ] Assign your self the merge request. Assign your Digital Experience onboarding buddy as the reviewer.
- [ ] Find the Digital Experience handbook page in the review app. Copy and paste the URL as a comment in your merge request. Notice the review app url starts with your branch name.
- [ ] Post in the #digital-experience Slack channel once your change is live on about.gitlab.com

## Set up coffee chats

Coffee chats are 15 minute informal zoom chats to meet and talk with team members. We have [handbook documentation on coffee chats here](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats). As part of your onboarding, let's schedule some coffee chats with Digital Experience team members. 

- [ ] Schedule 2 or 3 coffee chats with Digital Experience team members

/label ~"dex-status::todo"
