We are so excited you're here! 

The onboarding issue is divided into various tasks to get you set up as a Manager with the Digital Experience team. If you can't move forward with tasks please reach out to your onboarding buddy from the Digital Experience team.

This is a guide that should help you get started with the Digital Experience team at GitLab and some of the most important tools we use.

Your Digital Experience onboarding buddy is `insert-handle`. 

Complete this issue in tandem with your `Becoming a Manager: Name` GitLab issue.

## Team Structure

- [ ] Read our handbook https://about.gitlab.com/handbook/marketing/digital-experience/

## Resource

Slack channels to join
- `#dex-leadership` - Private channel for Digital Experience leaders
- `#digital-experience-team` - Primary channel for communicating to Digital Experience team
- `dex-fte` - Channel for fulltime team members
- `digitalexperience-water-cooler` - Channel unstructured non-work topics
- `dex-sns` - Search, Nav, and Support group communication
- `dex-conversion` - Conversion group comunication 
- `dex-productmarketing` - Product marketing group converstaion 

Please primarily use the #digital-experience-team slack channel for team communications. 