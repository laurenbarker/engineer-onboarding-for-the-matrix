# Navigation Repository Onboarding

We are so excited you're here! 

The onboarding issue is divided into various tasks to get you set with the Navigation Repo. 

Your Digital Experience onboarding buddy is `insert-handle`. The DRI for this repository is `@lduggan`.

Start this issue after completing: 
- your GitLab onboarding issue here: `GitLab Onboarding Issue`
- your Digital Experience onboarding issue here (if applicable) `Digital Experience Onboarding Issue`

## Navigation Repository

This repository contains the navigation and footer for the following:
- The old Marketing site (commonly called the `www` site) which also contains the [handbook](https://about.gitlab.com/handbook).
- The new Buyer Experience site, which we hope to move most of the marketing website over to.

Since both of the above repositories consume this navigation, you'll need to be onboarded into each of them in order to make Navigation changes. 

## Setup

- [ ] Complete your [Digital Experience onboarding](https://gitlab.com/gitlab-com/marketing/digital-experience/engineer-onboarding-for-digital-experience/-/blob/main/.gitlab/issue_templates/engineer-onboarding-issue.md), so that you have access to the [WWW repository](https://gitlab.com/gitlab-com/www-gitlab-com), and the [Buyer Experience repository](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience). 
- [ ] Clone the [Navigation repository](https://gitlab.com/gitlab-com/marketing/digital-experience/navigation) and run `npm install` for good measure.
- [ ] Follow the [README](https://gitlab.com/gitlab-com/marketing/digital-experience/navigation/-/blob/main/README.md) instructions to set up locally

## NPM Setup

Once you make changes to the Navigation repository, you'll need to publish those changes to NPM in order for the other repositories to access the updated package. 
_Note: For the most part, we will limit NPM admin access to engineers on Digital Experience. If that's not you, feel free to ping one of us, and we can publish to NPM for you._
- [ ] If you haven't already done so, create an [NPM account](https://www.npmjs.com/signup) using your GitLab email. Note: You'll have to type your login information blindly into a terminal, so try to make your password something secure, that you can also type easily.
- [ ] Request an admin or manager on the Digital Experience team to grant you publishing access. 

If you want to test your changes locally you can:
- Run `yarn link` (or `npm link`) in the root folder of this repository. This will create a local link to your package.
- Run `yarn link be-navigation` (or `npm link be-navigation`) in the root folder of the repository in which you want to test the package.

For more information about the `yarn link` command, please visit the [yarn docs](https://classic.yarnpkg.com/en/docs/cli/link).

/label ~"dex-status::todo"
